# -*- coding: utf-8 -*-
{
    "name": "Update writen wrong cost(income and expense both) of product on journal items",
    'summary': "Update cost journal. Cost = Income + Expense. Cost = Income and Expense both. Update journal. Cost update. Cost journal update. Update cost. Update journal items",
    "version": "13.0",
    "description": """
        If your journal items differs due to the cost of the products, you will be given the opportunity to correct each Product.
    """,
    "author": "Tb25",
    'support': 'torbatj79@gmail.com',
    "category": "Generic Modules",
    'price': 40.0,
    'currency': 'USD',
    "website": "",
    "license": "AGPL-3",
    'version': '13.0.1.0.0',
    'depends': ['product','account'],
    "update_xml": [
                'security/ir.model.access.csv',
                'views/update_product_cost_journal_items.xml',
             ],
    'images': ['static/src/img/banner.png'],
    "auto_install": False,
    "installable": True,
    "application": True,
}
