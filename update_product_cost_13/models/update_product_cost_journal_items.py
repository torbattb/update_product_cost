import logging
from odoo import api, exceptions, fields, models, tools, SUPERUSER_ID, _
from odoo.exceptions import UserError, AccessError, ValidationError
from odoo.tools.safe_eval import safe_eval
from odoo import api, fields, models, _
from odoo.addons import decimal_precision as dp
from odoo.exceptions import UserError, ValidationError
from odoo.tools import float_utils, float_compare
from datetime import datetime
import xlwt, xlrd, time, os, base64
from odoo.tools.float_utils import float_round
try:
    from StringIO import StringIO, BytesIO
except ImportError:
    from io import StringIO, BytesIO
_logger = logging.getLogger(__name__)
class UpdateProductCostJournalItems(models.TransientModel):
    _name = 'update.product.cost.journal.items'
    product_id = fields.Many2one('product.product', 'Product')
    journal_id = fields.Many2one('account.journal', 'Journal')
    update_cost = fields.Float('Update Cost', digits=(16, 2))
    start_date = fields.Date('Start date')
    end_date = fields.Date('End date')
    def update_more(self):
        items_obj = self.env['account.move.line']
        pickings_obj = self.env['stock.picking']
        picking_type_obj = self.env['stock.picking.type']
        update_items = items_obj.search([('product_id','=',self.product_id.id),('move_id.journal_id','=',self.journal_id.id),('date','>=',self.start_date),('date','<=',self.end_date),('quantity','!=',0.0)])
        if update_items:
            for line in update_items:
                if line.quantity > 0:
                    to_change = line.quantity * self.update_cost
                elif line.quantity < 0:
                    to_change = -1 * line.quantity * self.update_cost
                if line.debit > 0.0 and line.credit == 0.0:
                    query = '''UPDATE "account_move_line" SET debit = %s WHERE id = %s''' % (to_change, line.id)
                    self.env.cr.execute(query)
                elif line.debit == 0.0 and line.credit > 0.0:
                    query = '''UPDATE "account_move_line" SET credit = %s WHERE id = %s''' % (to_change, line.id)
                    self.env.cr.execute(query)
        picking_type_outgoings = picking_type_obj.search([('code','=','outgoing')])
        update_pickings = pickings_obj.search([('picking_type_id','in',picking_type_outgoings.ids),('date_done','>=',self.start_date),('date_done','<=',self.end_date),('move_ids_without_package','!=',None)])
        if update_pickings:
            for form_one_picking in update_pickings:
                for line_move_ids in form_one_picking.move_ids_without_package:
                    if line_move_ids.product_id == self.product_id:
                        query = '''UPDATE "stock_move" SET price_unit = %s WHERE id = %s''' % (self.update_cost, line_move_ids.id)
                        self.env.cr.execute(query)